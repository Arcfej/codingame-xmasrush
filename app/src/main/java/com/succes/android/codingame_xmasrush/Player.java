import java.util.*;
import java.io.*;
import java.math.*;
import java.util.stream.Collectors;

/**
 * Help the Christmas elves fetch presents in a magical labyrinth!
 **/
class Player {

    static final int NUMBER_OF_ROWS = 7;

    static int idCount;

    static List<List<Tile>> table;
    static Graph graph;

    static int playerX; // 0 - 6
    static int playerY; // 0 - 6

    static int opponentX; // 0 - 6
    static int opponentY; // 0 - 6

    static Tile playerTile;
    static Tile opponentTile;

    static Map<String, Item> items;
    static List<Item> playerQuests;

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            idCount = 0;

            // 0 - PUSH
            // 1 - MOVE
            int turnType = in.nextInt();

            // TABLE
            table = createTable(in);
            graph = makeGraph(table);

            // PLAYER
            int numPlayerCards = in.nextInt(); // 12
            playerX = in.nextInt();
            playerY = in.nextInt();
            // UP, RIGHT, DOWN, LEFT
            // 0 / 1
            playerTile = createTile(in.next(), -1, -1);

            // OPPONENT
            int numOpponentCards = in.nextInt(); // 12
            opponentX = in.nextInt();
            opponentY = in.nextInt();
            // UP, RIGHT, DOWN, LEFT
            // 0 / 1
            opponentTile = createTile(in.next(), -2, -2);

            // The total number of items available on board and on player tiles
            // 0 - 24
            int numItems = in.nextInt();
            items = getItems(in, numItems);

            // the total number of revealed quests for both players
            // 0 - 6
            int numQuests = in.nextInt();
            playerQuests = getPlayerQuests(in, numQuests);

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            // PUSH <id> <direction> | MOVE <direction> | PASS
            //     0 - 6        UP/DOWN/LEFT/RIGHT      MOVE   max 20
            if (turnType == 0) {
                push();
            } else {
                move();
            }
        }
    }

    static List<List<Tile>> createTable(Scanner in) {
        List<List<Tile>> table = new ArrayList<>(NUMBER_OF_ROWS);
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            List<Tile> row = new ArrayList<>(NUMBER_OF_ROWS);
            for (int j = 0; j < NUMBER_OF_ROWS; j++) {
                // UP, RIGHT, DOWN, LEFT
                // 0 / 1
                String tile = in.next();
                row.add(createTile(tile, j, i));
            }
            table.add(i, row);
        }
        return table;
    }

    static Tile createTile(String tile, int x, int y) {
        boolean up = tile.charAt(0) == '1';
        boolean right = tile.charAt(1) == '1';
        boolean down = tile.charAt(2) == '1';
        boolean left = tile.charAt(3) == '1';
        return new Tile(x, y, up, right, down, left);
    }

    static Graph makeGraph(List<List<Tile>> table) {
        List<Tile> tiles = new ArrayList<>(NUMBER_OF_ROWS * NUMBER_OF_ROWS);
        List<Path> paths = new ArrayList<>();
        for (int i = 0; i < table.size(); i++) {
            List<Tile> row = table.get(i);
            for (int j = 0; j < row.size(); j++) {
                Tile source = row.get(j);
                Tile leftDestination = j + 1 < row.size() ? row.get(j + 1) : null;
                Tile downDestination = i + 1 < table.size() ? table.get(i + 1).get(j) : null;
                tiles.add(source);
                if (source.right && leftDestination != null && leftDestination.left) {
                    paths.add(new Path(source, leftDestination));
                }
                if (source.down && downDestination != null && downDestination.up) {
                    paths.add(new Path(source, downDestination));
                }
            }
        }
        return new Graph(tiles, paths);
    }

    static Map<String, Item> getItems(Scanner in, int numItems) {
        Map<String, Item> items = new HashMap<>(numItems);
        for (int i = 0; i < numItems; i++) {
            String itemName = in.next();
            int itemX = in.nextInt(); // (-2) - 6   (-2: opponent, -1: player)
            int itemY = in.nextInt(); // (-2) - 6   (-2: opponent, -1: player)
            int itemPlayerId = in.nextInt(); // 0: player, 1: opponent
            Item item = new Item(itemPlayerId == 0, itemX, itemY, itemName);
            items.put(item.id.toString(), item);
        }
        return items;
    }

    static List<Item> getPlayerQuests(Scanner in, int numQuests) {
        List<Item> playerQuests = new ArrayList<>();
        for (int i = 0; i < numQuests; i++) {
            String questItemName = in.next();
            int questPlayerId = in.nextInt(); // 0: player, 1: opponent
            if (questPlayerId == 0) {
                String id = Item.ItemId.generateId(true, questItemName).toString();
                playerQuests.add(items.get(id));
            }
        }
        return playerQuests;
    }

    static void push() {
        String direction;
        int row;
        sortPlayerQuests();
        Item quest = playerQuests.get(0);
        if (Math.abs(quest.x - playerX) >= Math.abs(quest.y - playerY)) {
            if (quest.x - playerX < 0) direction = "RIGHT";
            else direction = "LEFT";
            row = quest.y < 0 ? 3 : quest.y;
        } else {
            if (quest.y - playerY < 0) direction = "DOWN";
            else direction = "UP";
            row = quest.x < 0 ? 3 : quest.x;
        }
        System.out.println("PUSH " + row + " " + direction);
    }

    static void move() {
        DijkstraAlgorithm routeFinder = new DijkstraAlgorithm(graph);

        List<Tile> moves = new ArrayList<>();
        sortPlayerQuests();
        List<Item> remainingQuests = playerQuests.stream()
                .filter(quest -> quest.x >= 0)
                .collect(Collectors.toList());

        while (remainingQuests.size() > 0 && moves.size() < 21) {
            routeFinder.execute(table.get(playerY).get(playerX));
            Item quest = remainingQuests.get(0);
            List<Tile> route = routeFinder.getRoute(table.get(quest.y).get(quest.x));
            if (route != null && route.size() > 0) {
                playerX = quest.x;
                playerY = quest.y;
                moves.addAll(route);
            }
            remainingQuests.remove(0);
        }

        if (moves.size() == 0) {
            TreeMap<Integer, List<Tile>> possibleRoutes = routeFinder.getReachableDistances();
            if (possibleRoutes.size() > 0) {
                moves = possibleRoutes.lastEntry().getValue();
            }
        }
        if (moves.size() > 21) {
            moves = moves.subList(0, 21);
        }
        System.out.println(getMoves(moves));
    }

    private static void sortPlayerQuests() {
        if (playerQuests.size() > 1) {
            playerQuests.sort((o1, o2) -> {
                double distance1 = Math.pow(o1.x - playerX, 2) + Math.pow(o1.y - playerY, 2);
                double distance2 = Math.pow(o2.x - playerX, 2) + Math.pow(o2.y - playerY, 2);
                return Double.compare(distance1, distance2);
            });
        }
    }

    static String getMoves(List<Tile> route) {
        if (route.size() == 0) return "PASS";
        StringBuilder moves = new StringBuilder("MOVE ");
        for (int i = 0; i < (route.size() - 1); i++) {
            Tile first = route.get(i);
            Tile second = route.get(i + 1);
            int diffX = second.x - first.x;
            int diffY = second.y - first.y;
            if (diffX == 1) moves.append("RIGHT ");
            else if (diffX == -1) moves.append("LEFT ");
            else if (diffY == 1) moves.append("DOWN ");
            else if (diffY == -1) moves.append("UP ");
        }
        return moves.toString().trim();
    }

    static class DijkstraAlgorithm {

        private final List<Tile> tiles;
        private final List<Path> paths;
        private Set<Tile> settledTiles;
        private Set<Tile> unsettledTiles;
        private Map<Tile, Tile> predecessors;
        private Map<Tile, Integer> distance;

        DijkstraAlgorithm(Graph graph) {
            this.tiles = new ArrayList<>(graph.tiles);
            this.paths = new ArrayList<>(graph.paths);
        }

        void execute(Tile source) {
            settledTiles = new HashSet<>();
            unsettledTiles = new HashSet<>();
            distance = new HashMap<>();
            predecessors = new HashMap<>();
            distance.put(source, 0);
            unsettledTiles.add(source);
            while (unsettledTiles.size() > 0) {
                Tile tile = getMinimum(unsettledTiles);
                settledTiles.add(tile);
                unsettledTiles.remove(tile);
                findMinimalDistances(tile);
            }
        }

        private Tile getMinimum(Set<Tile> tiles) {
            Tile minimum = null;
            for (Tile tile : tiles) {
                if (minimum == null) {
                    minimum = tile;
                } else {
                    if (getShortestDistance(tile) < getShortestDistance(minimum)) {
                        minimum = tile;
                    }
                }
            }
            return minimum;
        }

        private int getShortestDistance(Tile destination) {
            Integer d = distance.get(destination);
            return (d == null) ? Integer.MAX_VALUE : d;
        }

        private void findMinimalDistances(Tile tile) {
            List<Tile> adjacentTiles = getNeighbors(tile);
            for (Tile target : adjacentTiles) {
                if (getShortestDistance(target) > getShortestDistance(tile) + getDistance(tile, target)) {
                    predecessors.put(target, tile);
                    unsettledTiles.add(target);
                }
            }
        }

        private List<Tile> getNeighbors(Tile tile) {
            List<Tile> neighbors = new ArrayList<>();
            for (Path path : paths) {
                if (path.source == tile && !settledTiles.contains(path.destination)) {
                    neighbors.add(path.destination);
                } else if (path.destination == tile && !settledTiles.contains(path.source)) {
                    neighbors.add(path.source);
                }
            }
            return neighbors;
        }

        private int getDistance(Tile tile, Tile target) {
            return 1;
        }

        List<Tile> getRoute(Tile target) {
            List<Tile> route = new LinkedList<>();
            Tile step = target;
            if (predecessors.get(step) == null) {
                return null;
            }
            route.add(step);
            while (predecessors.get(step) != null) {
                step = predecessors.get(step);
                route.add(step);
            }
            Collections.reverse(route);
            return route;
        }

        List<Tile> getReachableTiles() {
            return new ArrayList<>(predecessors.keySet());
        }

        /**
         * @return a TreeMap<Distance, Route>
         */
        TreeMap<Integer, List<Tile>> getReachableDistances() {
            TreeMap<Integer, List<Tile>> targets = new TreeMap<>();
            for (Tile tile : getReachableTiles()) {
                List<Tile> route = getRoute(tile);
                if (route != null && route.size() > 1) {
                    targets.put(route.size(), route);
                }
            }
            return targets;
        }
    }

    static class Tile {
        int id;
        int x;
        int y;
        boolean right;
        boolean left;
        boolean up;
        boolean down;

        Tile(int x, int y, boolean up, boolean right, boolean down, boolean left) {
            this.id = idCount++;
            this.x = x;
            this.y = y;
            this.right = right;
            this.left = left;
            this.up = up;
            this.down = down;
        }

        @Override
        public String toString() {
            return x + "." + y + up + right + down + left;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tile tile = (Tile) o;
            return x == tile.x &&
            y == tile.y &&
            right == tile.right &&
            left == tile.left &&
            up == tile.up &&
            down == tile.down;
        }
    }

    static class Path {
        int id;
        Tile source;
        Tile destination;

        Path(Tile source, Tile destination) {
            this.id = idCount++;
            this.source = source;
            this.destination = destination;
        }

        @Override
        public String toString() {
            return source.toString() + " -> " + destination.toString();
        }
    }

    static class Graph {
        List<Tile> tiles;
        List<Path> paths;

        Graph(List<Tile> tiles, List<Path> paths) {
            this.tiles = tiles;
            this.paths = paths;
        }

        @Override
        public String toString() {
            return tiles.toString() + "\n" + paths.toString();
        }
    }

    static class Item {

        final ItemId id;
        final boolean player;
        final String name;
        int x;
        int y;

        Item(boolean player, int x, int y, String name) {
            id = ItemId.generateId(player, name);
            this.player = player;
            this.x = x;
            this.y = y;
            this.name = name;
        }

        @Override
        public String toString() {
            return id.toString() + x + y;
        }

        static class ItemId {

            final String id;

            private ItemId(boolean player, String itemName) {
                id = (player ? "0" : "1") + itemName;
            }

            public static ItemId generateId(boolean player, String itemName) {
                return new ItemId(player, itemName);
            }

            @Override
            public String toString() {
                return id;
            }
        }
    }
}